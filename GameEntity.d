﻿module GameEntity;

import derelict.sfml2.window;
import derelict.sfml2.graphics;
import derelict.sfml2.system;

import derelict.sfml2.windowtypes;
import derelict.sfml2.windowfunctions;

import derelict.sfml2.graphicsfunctions;
import derelict.sfml2.graphicstypes;

import std.exception;
import std.conv;
import std.string;
import std.stdio;
import std.array;

import Jaster.Entity : Animation;
import Entity;
import Game;
import NormalTiles;
import main;

class GameEntity : Entity
{
	public ubyte ID;

	public string[] OnPlayerTouchCommands;

	protected void Setup(sfTexture* texture, sfVector2f position, ubyte id)
	{
		this.SetTexture(texture);
		this.SetPosition(position);
		this.ID = id;
	}

	public abstract GameEntity Clone();

	public bool IsTouchingPlayer()
	{
		return(this.IsTouchingEntity(cast(Entity*)GameEntity.PlayerPointer));
	}

	public void OnPlayerTouch()
	{
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static GameEntity[] Entities;
	public static GameEntity[] EntityInfo;

	public static Player* PlayerPointer;

	public static void RegisterEntity(GameEntity entity)
	{
		GameEntity.Entities[entity.ID] = entity;
	}

	public static GameEntity GetEntity(int id)
	{
		return GameEntity.Entities[id].Clone();
	}

	public static void SetPlayer(Player* player)
	{
		this.PlayerPointer = player;
	}

	public static void UpdateEntities()
	{
		for(int i = 0; i < GameEntity.Entities.length; i++)
		{
			GameEntity.Entities[i].UpdateEntities();

			if(GameEntity.Entities[i].IsTouchingPlayer)
			{
				GameEntity.Entities[i].OnPlayerTouch();
			}
		}
	}
}